import ProyectoPruebas.settings

import pytest
from ProyectoPruebas.prueba.models import *


# Primera Tarea de la Iteraccion

@pytest.mark.django_db
def test_categoria():
    categoria = Categoria()
    categoria.nombre = 'Categoria'
    categoria.presentacion_comercial = 'Líquido'
    categoria.save()

    producto = Producto()
    producto.nombre = 'Producto 1'
    producto.existencia_minima = 10
    producto.existencia_maxima = 20
    producto.iva = "Si"
    producto.codigo = "SIAAF-001"
    producto.categoria = 1
    producto.save()

    assert producto.codigo == "SIAAF-001"