

from django.db import models



from django.forms import model_to_dict





class Categoria(models.Model):
    """
    Clase que representa  el conjunto de categorias para los productos
    """

    TIPO_LIQUIDOS= 'Líquido'
    TIPO_CAPSULAS= 'Cápsula'
    TIPO_OTROS= 'Otros'

    CHOICE_PRESENTACION = (
        (TIPO_LIQUIDOS, 'Líquido'),
        (TIPO_CAPSULAS, 'Cápsula'),
        (TIPO_OTROS, 'Otros'))


    nombre= models.CharField(max_length=20, verbose_name="Nombre",unique=True)
    presentacion_comercial = models.CharField(choices=CHOICE_PRESENTACION , verbose_name="Presentacion Comercial", max_length=18)

    class Meta:
        verbose_name = "Categoría"
        ordering = ['nombre']

    def toJSON(self):
        item = model_to_dict(self)
        item['nombre'] = self.nombre
        item['presentacion_comercial'] = self.presentacion_comercial
        return item

    def __str__(self):
        return self.nombre


class Proveedor(models.Model):
    """
    Clase que representa  el conjunto de proveedores para los productos
    """
    direccion = models.CharField(max_length=30, verbose_name="Direccion")
    email = models.EmailField(unique=True, verbose_name="Email")
    nombre = models.CharField(max_length=60, verbose_name="Nombre",unique=True)
    ruc= models.CharField(max_length=13, verbose_name="Ruc",unique=True)
    telefono = models.CharField(max_length=10,verbose_name="telefono celular",unique=True)

    class Meta:
        verbose_name = "Proveedor"
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        item['direccion'] = self.direccion
        item['email'] = self.email
        item['nombre'] = self.nombre
        item['ruc'] = self.ruc
        item['telefono'] = self.telefono
        return item

class Almacen(models.Model):
    """
    Clase que representa  el conjunto de almacenes para los productos
    """
    nombre= models.CharField(max_length=20, verbose_name="Nombre",unique=True)

    class Meta:
        verbose_name = "Almacén"
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

    def toJSON(self):
        item = model_to_dict(self)
        item['nombre'] = self.nombre
        return item

class Producto(models.Model):

    TIPO_IVA = 'Si'
    TIPO_NOIVA = 'No'

    CHOICE_IVA= (
        (TIPO_IVA, 'Si'),
        (TIPO_NOIVA, 'No'),)


    codigo = models.CharField(max_length=20, verbose_name="Codigo", unique=True)
    existencia_maxima = models.IntegerField(verbose_name="Existencia Máxima")
    existencia_minima = models.IntegerField(verbose_name="Existencia Mínima")
    nombre = models.CharField(max_length=20, verbose_name="Nombre", unique=True)
    iva = models.CharField(choices=CHOICE_IVA , verbose_name="Presentacion Comercial", max_length=18)

    categoria = models.ForeignKey(Categoria, related_name='categoria_producto', on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Producto"
        ordering = ['nombre']

    def toJSON(self):
        item = model_to_dict(self)
        item['codigo'] = self.codigo
        item['existencia_maxima'] = self.existencia_maxima
        item['existencia_minima'] = self.existencia_minima
        item['nombre'] = self.nombre
        item['categoria'] = self.categoria.toJSON()
        return item

    def __str__(self):
        return self.nombre


class Movimiento(models.Model):
    """
    Clase que representa  el conjunto de movimientos
    """
    TIPO_ENTRADA = 'Entrada'
    TIPO_SALIDA= 'Salida'


    CHOICE_TIPO = (
        (TIPO_ENTRADA, 'Entrada'),
        (TIPO_SALIDA, 'Salida'))

    cantidad = models.IntegerField(verbose_name="Cantidad")
    e_cantidad = models.IntegerField(verbose_name="Stock")
    e_valor_unitario = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Existencias valor  unitario ")
    e_valor_total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Existencias valor total")
    fecha = models.DateField(verbose_name="Fecha")
    tipo = models.CharField(choices=CHOICE_TIPO, verbose_name="Tipo", max_length= 10)
    valor_total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor Total")
    valor_unitario = models.DecimalField(max_digits=6, decimal_places=3, verbose_name="Valor Unitario")

    producto = models.ForeignKey(Producto, related_name='producto_movimiento', on_delete=models.PROTECT)
    #responsable = models.ForeignKey(Persona, related_name='usuario_movimiento',on_delete=models.CASCADE, null=True)

    class Meta:
        verbose_name = "Movimiento"
        ordering = ['fecha']

    def __str__(self):
        return self.tipo

    def toJSON(self):
        item = model_to_dict(self)
        item['cantidad'] = self.cantidad
        item['e_cantidad'] = self.e_cantidad
        item['e_valor_unitario'] = self.e_valor_unitario
        item['e_valor_total'] = self.e_valor_total
        # item['fecha'] = self.created_at.strftime("%Y/%m/%d")
        item['fecha'] = self.fecha
        item['tipo'] = self.tipo
        item['valor_total'] = self.valor_total
        item['valor_unitario'] = self.valor_unitario
        item['producto'] = self.producto.toJSON()
        return item

class Stock(models.Model):
    cantidad_existente = models.PositiveIntegerField(verbose_name="Stock")
    fecha = models.DateTimeField(auto_now_add=True, null=True, verbose_name="fecha")
    precio_unitario = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor  unitario actual")
    precio_total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor total actual")

    movimiento = models.ForeignKey(Movimiento, related_name='movimiento_stock', on_delete=models.PROTECT)
    producto = models.ForeignKey(Producto, related_name='producto_stock', on_delete=models.PROTECT)
    almacen = models.ForeignKey(Almacen, related_name='almacen_stock', on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Stock"
        ordering = ['-fecha']

    def toJSON(self):
        item = self.producto.toJSON()
        item['cantidad'] = self.cantidad_existente
        item['precio_unitario'] = self.precio_unitario
        item['precio_total'] = self.precio_total
        item['nombre'] = self.producto.nombre
        item['categoria'] = self.producto.categoria.nombre
        item['producto'] = self.producto.toJSON()
        return item

    def __str__(self):
        return 'Almacen: {}, Producto: {}, Stock: {}'.format(self.almacen.nombre, self.producto.nombre, self.cantidad_existente)

class OrdenEntrada(models.Model):
    """
    Clase que representa  el conjunto de ordenes de entrada para los productos
    """
    TIPO_BORRADOR = 'Borrador'
    TIPO_VALIDADA= 'Validada'

    CHOICE_ESTADO = (
        (TIPO_BORRADOR, 'Borrador'),
        (TIPO_VALIDADA, 'Validada'))

    estado = models.CharField(choices=CHOICE_ESTADO, verbose_name="Estado", max_length=18)

    fecha= models.DateField(verbose_name="Fecha",blank=True, null=True)
    iva=models.DecimalField(max_digits=7, decimal_places=3,verbose_name="Iva")
    nro_compra = models.IntegerField(verbose_name="Nro de compra", unique=True,)
    sub_total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Sub Total")
    total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Total")

    almacen = models.ForeignKey(Almacen, related_name='almacen_orden_entrada', on_delete=models.PROTECT)
    proveedor = models.ForeignKey(Proveedor, related_name='proveedor_orden_entrada', on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Órden de entrada"
        ordering = ['nro_compra']

    def __str__(self):
        return 'N° Órden: {} con Estado: {}'.format(self.nro_compra, self.estado)

    def toJSON(self):
        item = model_to_dict(self)
        item['estado'] = self.estado
        item['fecha'] = self.fecha
        item['iva'] = self.iva
        item['nro_compra'] = self.nro_compra
        item['sub_total'] = self.sub_total
        item['total'] = self.total
        item['almacen'] = self.almacen.toJSON()
        item['proveedor'] = self.proveedor.toJSON()
        return item


class ItemEntrada(models.Model):
    """
    Clase que representa  el conjunto de items de entrada
    """
    cantidad_basica = models.IntegerField(verbose_name="Cantidad unidad básica")
    cantidad_total = models.IntegerField(verbose_name="Cantidad en total")
    valor_total=models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor Total")
    valor_unitario = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor Unitario")

    orden_entrada = models.ForeignKey(OrdenEntrada, related_name='orden_entrada_item_entrada', on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, related_name='producto_item_entrada', on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Item de entrada"

    def __str__(self):
        return 'Producto: {} con Órden Entrada: {}'.format(self.producto.nombre, self.orden_entrada.nro_compra)

    def toJSON(self):
        item = model_to_dict(self)
        item['cantidad_basica'] = self.cantidad_basica
        item['cantidad_total'] = self.cantidad_total
        item['valor_total'] = self.valor_total
        item['valor_unitario'] = self.valor_unitario
        item['orden_entrada'] = self.orden_entrada.toJSON()
        item['producto'] = self.producto.toJSON()
        return item

class OrdenSalida(models.Model):
    """
    Clase que representa  el conjunto de ordenes de salida para los productos

    """

    #estado = models.BooleanField(default=False, verbose_name="Estado")
    TIPO_BORRADOR = 'Borrador'
    TIPO_VALIDADA= 'Validada'

    CHOICE_ESTADO = (
        (TIPO_BORRADOR, 'Borrador'),
        (TIPO_VALIDADA, 'Validada'))


    estado = models.CharField(choices=CHOICE_ESTADO, verbose_name="Estado", max_length=18)

    fecha= models.DateField(verbose_name="Fecha")
    nro_orden_salida = models.IntegerField(verbose_name="Nro de orden de Salida", unique=True)
    sub_total=models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Sub Total")
    total = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Total")

    almacen = models.ForeignKey(Almacen, related_name='almacen_orden_salida', on_delete=models.PROTECT)
    #consulta = models.ForeignKey(Consulta, related_name='consulta_orden_salida', null=True, blank= True, on_delete=models.PROTECT)
    departamento = models.ForeignKey(Almacen, related_name='departameto_orden_salida', on_delete=models.PROTECT)
    #usuario=models.ForeignKey(Persona, related_name='usuario_orden_salida',on_delete=models.CASCADE)


    class Meta:
        verbose_name = "Órden de salida"
        ordering = ['nro_orden_salida']

    def __str__(self):
        return 'N° Órden: {} con Estado: {}'.format(self.nro_orden_salida, self.estado)

    def toJSON(self):
       item = model_to_dict(self)
       item['estado'] = self.estado
       item['fecha'] = self.fecha
       item['nro_orden_salida'] = self.nro_orden_salida
       item['sub_total'] = self.sub_total
       item['total'] = self.total
       item['almacen'] = self.almacen.toJSON()
       #item['consulta'] = self.consulta.toJSON()
       #item['departamento'] = self.departamento.toJSON()
       #item['usuario'] = self.usuario.toJSON()
       return item

class ItemSalida(models.Model):
    """
    Clase que representa  el conjunto de items de salida
    """

    cantidad_basica = models.IntegerField(verbose_name="Cantidad unidad básica")
    cantidad_total = models.IntegerField(verbose_name="Cantidad en total")
    valor_total = models.DecimalField(max_digits=9, decimal_places=3,verbose_name="Valor Total")
    valor_unitario = models.DecimalField(max_digits=9, decimal_places=3, verbose_name="Valor Unitario")

    orden_salida = models.ForeignKey(OrdenSalida, related_name='orden_salida_item_salida', on_delete=models.CASCADE)
    producto = models.ForeignKey(Producto, related_name='producto_item_salida', on_delete=models.PROTECT)

    class Meta:
        verbose_name = "Item de salida"

    def __str__(self):
        return 'Producto: {} con Órden Salida: {}'.format(self.producto.nombre, self.orden_salida.nro_orden_salida)

    def toJSON(self):
        item = model_to_dict(self)
        item['cantidad_basica'] = self.cantidad_basica
        item['cantidad_total'] = self.cantidad_total
        item['valor_total'] = self.valor_total
        item['valor_unitario'] = self.valor_unitario
        item['orden_salida'] = self.orden_salida.toJSON()
        item['producto'] = self.producto.toJSON()
        return item



