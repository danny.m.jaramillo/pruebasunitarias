
from django.contrib import admin

# Register your models here.


#Inventario
from ProyectoPruebas.prueba.models import *

admin.site.register(Producto)
admin.site.register(Proveedor)
admin.site.register(OrdenSalida)
admin.site.register(Movimiento)
admin.site.register(Stock)
admin.site.register(OrdenEntrada)
admin.site.register(ItemSalida)
admin.site.register(ItemEntrada)
admin.site.register(Almacen)
admin.site.register(Categoria)



# Register your models here.
